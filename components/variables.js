//---------------------index variables -----------------//
export const container = document.querySelector("#root");
export const leftText = document.createElement("div");
export const leftTextContainer = document.createElement("div");
export const rightForm = document.createElement("div");
export const cloud = document.createElement("img");
export const earth = document.createElement("img");
export const shieldCont = document.createElement("div");
export const rightFormContainer = document.createElement("div");
export const form = document.createElement("form");
export const inputSecond = document.createElement("input");
export const formContainerTextFirst = document.createElement("div");
export const backgroundContainer = document.createElement("div");
export const enterLink = document.createElement("a");
export const inputFirst = document.createElement("input");
export const buttonContainer = document.createElement("div");
export const buttonSignIn = document.createElement("button");
export const enterContainer = document.createElement("div");
export const shield = document.createElement("div");
export const enterText = document.createElement("p");
export const shildText = document.createElement("p");
export const windowContainer = document.createElement("div");
export const windowContainerText = document.createElement("p");
export const profileContainer = document.createElement("div");
export const birthday = document.createElement("input");
//---------------------index variables -----------------//

//---------------------game variables -----------------//



export const current = JSON.parse(localStorage.getItem("currentUser"));
export const gameContainer = document.querySelector("#game");
export const rightProfileContainer = document.createElement("div");
export const imageProfileContainer = document.createElement("div");
export const exitContainer = document.createElement("div");

export let editContainer = document.createElement("div");
export const windowContainerGame = document.createElement("div");
export const buttonSettings = document.createElement("button");

//---------------------game variables -----------------//