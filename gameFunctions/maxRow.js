import * as vars from '../components/variables.js';
import { createAvatar } from './createAvatar.js';
export function createWindowSettings() {
    const leftSettings = document.createElement("div");
    const rightSettings = document.createElement("div");
    let containerSet = document.createElement("div");
    containerSet.className = "containerSet"
    containerSet.append(leftSettings, rightSettings);

    vars.windowContainerGame.className = "modal-settings";
    const windowText = document.createElement("p");
    windowText.innerText = "Настройки";
    vars.windowContainerGame.append(containerSet);
    vars.gameContainer.append(vars.windowContainerGame);
    leftSettings.className = "leftSettings";
    rightSettings.className = "rightSettings";
    rightSettings.style.width = "270px";
    leftSettings.append(createAvatar());
    settingsRow2();

    vars.buttonSettings.className = "button-set";
    vars.buttonSettings.innerText = "Сохранить";
    vars.windowContainerGame.append(vars.buttonSettings);
    vars.windowContainerGame.style.display = "none";
    let infos = Array.from(document.querySelectorAll(".info2"));
    let [newName, password, image] = infos;
    /* vars.current.currentMoney = 1000; */

    function setInfo() {

        vars.current.name = newName.value;
        vars.current.password = password.value;
        vars.current.src = image.value;
        /* vars.current.currentMoney = 1000; */


        localStorage.setItem("currentUser", JSON.stringify(vars.current));
    }
    vars.buttonSettings.addEventListener("click", () => {
        setTimeout(setInfo, 400);
        vars.windowContainerGame.style.display = "none";
    });

    function settingsRow2() {
        function row(nameof, valueof) {

            let leftSide = document.createElement("div");
            leftSide.innerText = nameof;
            let rightSide = document.createElement("input");
            rightSide.innerText = valueof;
            rightSide.value = valueof;
            rightSide.className = "info2";

            let contsides = document.createElement("div");
            contsides.className = "row-container-profile";
            contsides.append(leftSide, rightSide);
            rightSettings.append(contsides);
        }
        row("Имя", vars.current.name);
        row("Пароль", vars.current.password);
        row("Аватарка", vars.current.src);
    }

}