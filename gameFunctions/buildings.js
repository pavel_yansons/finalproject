export function buildings(building, description, cost) {
    const buildingContainer = document.createElement("div");
    buildingContainer.className = "building-container";
    const buildingImage = document.createElement("div");
    buildingImage.className = "building-image";
    const image = document.createElement("img");
    image.setAttribute("src", building);
    image.className = "build-img";

    const buildingDesc = document.createElement("p");
    const buildingCost = document.createElement("p");
    buildingDesc.innerText = description;
    buildingCost.innerText = cost;
    buildingContainer.append(buildingImage, buildingDesc, buildingCost);
    buildingImage.append(image);
    return buildingContainer;
}