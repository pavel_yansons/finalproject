import * as vars from '../components/variables.js';
import { edit } from './settingsRow.js';
import { exitFromAcc } from './exitFunction.js';
import { moneyValue, moneyBlock } from './money.js';
import { timer, createTimeBlock, seconds, minutes, timeValue } from './timer.js';
import { createWindowSettings } from './maxRow.js';
import { createAvatar } from './createAvatar.js';
import { createExit, changeIcon, changeBack } from './createExit.js';
import { buildings } from './buildings.js';
import { gameListeners } from './gameListeners.js';

export function createProfile() {
    const profileContainer = document.createElement("div");
    profileContainer.className = "profile-container-game";
    const leftProfileContainer = document.createElement("div");

    leftProfileContainer.className = "leftSide-profile-container";
    vars.rightProfileContainer.className = "rightSide-profile-container";

    vars.imageProfileContainer.className = "image-profile-container";
    console.log(vars.current);
    leftProfileContainer.append(vars.imageProfileContainer);
    profileContainer.append(leftProfileContainer, vars.rightProfileContainer);
    vars.gameContainer.append(profileContainer);
    vars.imageProfileContainer.append(createAvatar());
    maxRow();
}

function maxRow() {
    function row(nameof, valueof) {

        let leftSide = document.createElement("div");
        leftSide.innerText = nameof;
        let rightSide = document.createElement("div");
        rightSide.innerText = valueof;
        rightSide.value = valueof;
        rightSide.className = "info";

        let contsides = document.createElement("div");
        contsides.className = "row-container-profile";
        contsides.append(leftSide, rightSide);
        vars.rightProfileContainer.append(contsides);
    }
    row("Имя", vars.current.name);

    row("Возраст", vars.current.age);
};

const itemsObj = {
    apple: {

        name: "Яблоня",
        cost: 10,
        img: './images/apple.png',
        moneyBoost: function() {
            vars.current.currentMoney++;
        },
        moneyFunction: function() {
            setInterval(this.moneyBoost, 1000);
        },
        maxLength: 2,
        id: 0,

    },
    cock: {

        name: "Петушок",
        cost: 50,
        img: './images/petushok.png',
        moneyBoost: function() {
            vars.current.currentMoney += 3;
        },
        moneyFunction: function() {
            setInterval(this.moneyBoost, 1000);
        },
        maxLength: 2,
        id: 1,
    },
    chiken: {

        name: "Курочка",
        cost: 20,
        img: './images/kurochka.png',
        moneyBoost: function() {
            vars.current.currentMoney += 2;
        },
        moneyFunction: function() {
            setInterval(this.moneyBoost, 1000);
        },
        maxLength: 2,
        id: 2,
    },
    kurochkabuh: {

        name: "Бухгалтер",
        cost: 250,
        img: './images/kurochkabuh.png',
        moneyBoost: function() {
            vars.current.currentMoney += 5;
        },
        moneyFunction: function() {
            setInterval(this.moneyBoost, 1000);
        },
        maxLength: 2,
        id: 3,
    },
    chickenСoop: {

        name: "Курятник",
        cost: 1000,
        img: './images/kuryatnik.png',
        moneyBoost: function() {
            vars.current.currentMoney += 10;
        },
        moneyFunction: function() {
            setInterval(this.moneyBoost, 10);
        },
        maxLength: 2,
        id: 4,
    }
};
const array = Object.values(itemsObj);


let itemsBuildingsContainer = document.createElement("div");
itemsBuildingsContainer.className = "buildings";
vars.gameContainer.append(itemsBuildingsContainer);
itemsBuildingsContainer.append(buildings(itemsObj.apple.img, itemsObj.apple.name, itemsObj.apple.cost));
itemsBuildingsContainer.append(buildings(itemsObj.cock.img, itemsObj.cock.name, itemsObj.cock.cost));
itemsBuildingsContainer.append(buildings(itemsObj.chiken.img, itemsObj.chiken.name, itemsObj.chiken.cost));
itemsBuildingsContainer.append(buildings(itemsObj.kurochkabuh.img, itemsObj.kurochkabuh.name, itemsObj.kurochkabuh.cost));
itemsBuildingsContainer.append(buildings(itemsObj.chickenСoop.img, itemsObj.chickenСoop.name, itemsObj.chickenСoop.cost));
let gameStrict = document.createElement("div");

function createObj(obj) {
    const objectBlock = document.createElement("img");
    objectBlock.setAttribute('src', obj.img);
    gameStrict.append(objectBlock);
}

function createGame() {

    gameStrict.className = "polyana";

    vars.gameContainer.append(gameStrict);
}


const i = 0;
const arrBuildings = Array.from(document.querySelectorAll(".building-container"));
arrBuildings.forEach((item, index) => {
    item.setAttribute('data-id', index);
});
vars.current.currentMoney = 10;
let countChild = 0;

function buyBuilding() {

    arrBuildings.forEach((item) => item.addEventListener('click', (event) => {

        let elem = array.find((item) => item.id == event.currentTarget.dataset.id);
        let result = vars.current.currentMoney - elem.cost;

        if (result < 0) {
            return;
        } else {
            if (countChild < 12) {
                countChild++;
                vars.current.currentMoney = vars.current.currentMoney - elem.cost;

                console.log(countChild);
                elem.moneyFunction();
                createObj(elem);
                if (elem.name == "Курятник") {
                    alert("Вы выиграли");
                }
            } else {
                alert("вы создали максимальное количество элементов на поле");
            };
        }
    }));

}
setInterval(() => {
    moneyValue.innerHTML = vars.current.currentMoney;
}, 10);
setInterval(timer, 1000)
setInterval(() => {
    timeValue.innerText = `${minutes} : ${seconds}`;
}, 10);
vars.current.currentMoney = 100;
buyBuilding();
createGame();
moneyBlock();
createWindowSettings();
createTimeBlock();
edit();
gameListeners();
createExit("<i class=\"fas fa-door-closed\" ></i>");