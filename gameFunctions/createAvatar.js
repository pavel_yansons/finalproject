import * as vars from '../components/variables.js';
export function createAvatar() {
    const avatarBlock = document.createElement("div");
    avatarBlock.className = "avatar";
    avatarBlock.style.backgroundImage = `url(${vars.current.src})`;
    return avatarBlock;
}