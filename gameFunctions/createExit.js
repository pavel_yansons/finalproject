import * as vars from '../components/variables.js';
const iconExit = document.createElement("div");
const exitText = document.createElement("p");
export function createExit(icontags) {

    vars.exitContainer.className = "exit-container";

    iconExit.innerHTML = icontags;

    exitText.innerText = "Выход";
    vars.gameContainer.append(vars.exitContainer);
    vars.exitContainer.append(iconExit, exitText);
    return vars.exitContainer

}

export let changeIcon = createExit.bind(this, "<i class=\"fas fa-door-closed\" ></i>");
export let changeBack = createExit.bind(this, "<i class=\"fas fa-door-open\" style=\"color: #42275a;\"></i>");