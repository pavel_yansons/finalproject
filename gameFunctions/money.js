export const moneyValue = document.createElement("p");
import * as vars from '../components/variables.js';
export function moneyBlock() {
    const moneyContainer = document.createElement("div");
    moneyContainer.className = "money-container";
    const moneyIcon = document.createElement("div");
    moneyIcon.innerHTML = "<i class=\"fas fa-money-bill-alt\"></i>";
    moneyIcon.className = "moneyIcon-container";

    moneyValue.innerText = vars.current.currentMoney;
    moneyContainer.append(moneyIcon, moneyValue);
    vars.gameContainer.append(moneyContainer);
}