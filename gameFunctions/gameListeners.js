import * as vars from '../components/variables.js';
import { exitFromAcc } from './exitFunction.js';
import { createExit, changeIcon, changeBack } from './createExit.js';
export function gameListeners() {
    function hide() {
        vars.windowContainerGame.style.display = "block";
    }
    vars.exitContainer.addEventListener("click", exitFromAcc);
    vars.exitContainer.addEventListener("mouseover", changeBack);
    vars.exitContainer.addEventListener("mouseout", changeIcon);
    vars.editContainer.addEventListener("click", hide);
}