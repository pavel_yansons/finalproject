import * as vars from '../components/variables.js';

export let seconds = 0;
export let minutes = 0;
export let timeValue = document.createElement("p");
export function timer() {


    if (seconds >= 59) {
        minutes++;
        seconds = 0;
    } else seconds++;

}
export function createTimeBlock() {
    const timeContainer = document.createElement("div");
    timeContainer.className = "time-container";
    const timeIcon = document.createElement("div");
    timeIcon.innerHTML = "<i class=\"far fa-clock\"></i>";
    timeIcon.className = "timeIcon-container";

    timeValue.innerText = `${minutes} : ${seconds}`;
    let finalTime = minutes * 60 + seconds;
    timeContainer.append(timeIcon, timeValue);
    vars.gameContainer.append(timeContainer);
    return finalTime;
}