import * as vars from '../components/variables.js';

export function edit() {

    vars.editContainer.className = "edit-container";
    vars.editContainer.innerHTML = "<i class=\"fas fa-cogs\"></i>";
    vars.gameContainer.append(vars.editContainer);

}