import * as vars from '../components/variables.js';
export function exitFromAcc() {
    localStorage.setItem(vars.current.name, JSON.stringify(vars.current));
    localStorage.setItem("currentUser", JSON.stringify(null));

    window.location.href = "./index.html";
}