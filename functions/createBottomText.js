import * as vars from '../components/variables.js';

export function createBottomText(visible) {
    vars.enterLink.innerText = "Войти";
    vars.enterLink.style.display = "block";
    vars.enterContainer.style.display = visible;
    vars.enterContainer.style.justifyContent = "center";
    vars.enterText.style = "    font-size: 14px;padding-top: 15px;color: dimgray;"
    vars.enterText.innerText = "Уже есть аккаунт?";
    vars.enterLink.className = "link-active"
    vars.enterLink.style = "font-size: 14px;padding-top: 15px;color: dimgray;padding-left: 20px; cursor: pointer;";
    vars.enterContainer.append(vars.enterText, vars.enterLink);
    vars.form.append(vars.enterContainer);
}