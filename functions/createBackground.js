import * as vars from '../components/variables.js';
import { animations } from '../functions/animation.js';
export function createBackground() {
    vars.backgroundContainer.classList = "page-container";
    vars.leftText.className = "left";
    vars.cloud.className = "cloud";
    vars.cloud.setAttribute("src", "images/cloud.png");
    vars.earth.className = "earth";
    vars.earth.setAttribute("src", "images/kisspng-lawn-grassland-sky-atmosphere-wallpaper-green-and-fresh-grass-border-texture-5aa7c25ba5ac65.0860494215209437076786.png");
    vars.leftTextContainer.className = "left-text__container";
    vars.leftTextContainer.innerHTML = "<p>Поднимай страну с колен!</p><p>Строй своё государство! </p>"
    vars.rightForm.className = "right";
    vars.container.append(vars.backgroundContainer);
    vars.backgroundContainer.append(vars.leftText, vars.rightForm, vars.cloud, vars.earth);
    vars.leftText.append(vars.leftTextContainer);

    animations();
}