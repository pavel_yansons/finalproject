import * as vars from '../components/variables.js';
import { logIn } from './logIn.js';

vars.windowContainer.className = "windowContainer";
let containerOver = document.createElement("div");

export function createWindow(currentUser) {
    vars.rightFormContainer.style.display = "none";
    vars.leftTextContainer.style.display = "none";

    vars.windowContainerText.innerText = `Вы вошли как: `;
    vars.container.append(vars.windowContainer);
    vars.windowContainer.append(vars.windowContainerText, vars.profileContainer);

    const profileImg = document.createElement("div");
    profileImg.className = "img-cotntainer";
    vars.profileContainer.className = "profile-container";
    profileImg.innerHTML = "<i class=\"fas fa-user-tie\"></i>";
    vars.profileContainer.append(profileImg, containerOver);
    containerOver.className = "containerOver";
    createRow("Имя", currentUser.name);
    createRow("Деньги", currentUser.currentMoney);
    createRow("Уровень", currentUser.currentLevel);
    createRow("Текущий опыт", currentUser.currentLevel);
    createPlayButton();
}

export function createRow(nameof, valueof) {

    let leftSide = document.createElement("div");
    leftSide.innerText = nameof;
    let rightSide = document.createElement("div");
    rightSide.innerText = valueof;
    /* let mainContainer = document.createElement("div"); */
    let contsides = document.createElement("div");
    contsides.className = "row-container";
    contsides.append(leftSide, rightSide);

    /* containerOver.append(contsides); */
    containerOver.append(contsides);

}

function createPlayButton() {
    let buttonContainerPlay = document.createElement("div");
    let buttonPlay = document.createElement("div");
    buttonPlay.classList = "buttonPlay";
    buttonPlay.style.display = "flex";
    const textInButton = document.createElement("div");
    textInButton.innerText = "Поднять страну!"
    const iconInButton = document.createElement("div");
    iconInButton.innerHTML = "<i class=\"fas fa-place-of-worship\"></i>";
    buttonPlay.append(textInButton, iconInButton);
    /* buttonPlay.innerText = `Поднять страну! `; */
    buttonContainerPlay.append(buttonPlay);
    vars.windowContainer.append(buttonContainerPlay);
}