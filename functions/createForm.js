import * as vars from '../components/variables.js';
import { registration, reg } from './registration.js';
import { createBottomText } from './createBottomText.js';
import { listeners } from './formListeners.js';
//-----------------------------imports-------------------------------//
export function createForm(status, id, icon, opacity) {
    vars.rightFormContainer.className = "form-container";
    vars.form.className = "form"
    vars.rightForm.append(vars.rightFormContainer);
    vars.rightFormContainer.append(vars.form);
    vars.formContainerTextFirst.innerHTML = "<i class=\"fas fa-user-check\"></i>";
    vars.inputFirst.setAttribute("placeholder", "Введите логин");
    vars.inputSecond.setAttribute("placeholder", "Введите пароль");
    vars.inputFirst.className = "login";
    vars.inputFirst.type = "text";
    vars.inputSecond.className = "password";
    vars.inputSecond.type = "password";
    vars.buttonContainer.className = "button-container";
    vars.birthday.setAttribute("type", "date");
    vars.birthday.className = "birthday";
    vars.birthday.style.opacity = opacity;
    [vars.formContainerTextFirst, vars.inputFirst, vars.inputSecond, vars.birthday, vars.shield, vars.buttonContainer].forEach((item) => vars.form.appendChild(item));
    //---------------buttons----------------------//
    vars.shield.style.opacity = icon;
    vars.buttonSignIn.className = "button";
    vars.buttonSignIn.innerText = status;
    vars.buttonSignIn.id = id;
    vars.buttonContainer.append(vars.buttonSignIn);
    vars.shield.className = "shield";
    vars.shildText.className = "shield-text"
    vars.shield.append(vars.shildText);
    createBottomText("flex");
    registration();
    listeners();

}