import * as vars from '../components/variables.js';
import { compare } from './compare.js';
import { logIn } from './logIn.js';
import { createForm } from './createForm.js';
import { registration, reg } from './registration.js';
import { createBottomText } from './createBottomText.js';
export function listeners() {
    let changing = createForm.bind(this, "Войти", "none", "0", "0");

    function replaceEventListener() {
        vars.buttonSignIn.removeEventListener("click", reg);
        vars.buttonSignIn.addEventListener("click", logIn);
    }
    vars.enterLink.addEventListener("click", () => {
        changing();
        replaceEventListener();
        createBottomText("none");
    });
    vars.inputSecond.addEventListener("input", compare);
}