import * as vars from '../components/variables.js';


let offset = 0;
let opacity = 0;
export function animations() {
    function animationForm() {
        opacity += 10
        offset += 1;
        vars.rightFormContainer.style.marginLeft = offset + "px";
        vars.rightFormContainer.style.opacity = opacity + "%";
        if (offset > 40) {
            return true
        }
        setTimeout(animationForm, 50);
    }
    animationForm();

    function textAnimation() {
        opacity += 1
        offset += 1;
        vars.leftTextContainer.style.marginTop = offset + "px";
        vars.leftTextContainer.style.opacity = opacity + "%";
        if (offset > 40) {
            return true
        }
        setTimeout(textAnimation, 50);
    }
    textAnimation();
}