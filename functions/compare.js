import * as vars from '../components/variables.js';


function createDenied() {
    vars.shield.classList.add("denied");
    const sad = document.createElement("div");
    sad.innerHTML = "<i class=\"far fa-frown\"></i>";
    const sadText = document.createElement("p");
    sadText.innerText = "Слабый пароль";
    const sadTextLast = document.createElement("p");
    vars.shildText.style.display = "none";
    sad.classList = "sad";
    vars.shieldCont.append(sadText, sad, sadTextLast);
    vars.shield.append(vars.shieldCont);
}
let counter = 0;

function success() {
    const smile = document.createElement("div");
    const smileText = document.createElement("p");
    smileText.innerText = "Классный пароль!";
    smile.innerHTML = "<i class=\"far fa-smile-wink\"></i>";
    vars.shield.append(smileText, smile);
}

export function compare() {
    if (vars.inputSecond.value.length < 4 && counter < 1) {
        createDenied();
        counter++;
    } else if (vars.inputSecond.value.length >= 4 && counter <= 1) {
        vars.shieldCont.style.display = "none";
        counter++;
        success();
    };
}