import * as vars from '../components/variables.js';
import { createWindow } from './entercheck.js';

let currentUser;

export function logIn(event) {
    event.preventDefault();
    currentUser = JSON.parse(localStorage.getItem(vars.inputFirst.value));
    if (currentUser == null) {
        vars.inputFirst.style.boxShadow = " rgba(133, 46, 46, 1) 0px 0px 20px"
    } else if (vars.inputSecond.value !== currentUser.password && currentUser !== null) {
        vars.inputSecond.style.boxShadow = " rgba(133, 46, 46, 1) 0px 0px 20px";
    } else {
        createWindow(currentUser, vars.container);
        window.location.href = "./game.html";
    };
    console.log(currentUser);
    localStorage.setItem("currentUser", JSON.stringify(currentUser));

    return currentUser;

}